//
//  Main.swift
//  MAD
//
//  Created by Тест on 30.10.2021.
//

import SwiftUI

struct Main: View {
    @Binding var transition:Int
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct Main_Previews: PreviewProvider {
    static var previews: some View {
        Main(transition: .constant(1))
    }
}
